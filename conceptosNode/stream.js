/**
 * Son abstracciones las cuales me permiten trabajar con flujos de informacion ya sea
de lectura o escritura puede ser asincrona.
Se usa en node para hacer envio y recepcion de informacion que es binaria o buffers, maneja
internamente el backpresure(se hacen por pequeños pedasos)
la mayoria de los strems son 'eventEmitters' (emisores de eventos) la ventaja es que las
operaciones se pueden encadenar usando el concepto pipes, aqui se tiene stoud(standart output) y lo envio
a al standart input (stint) de otra funcion, normalmente solo puedo hacer un pipe con un documento
de escritura a lectura o viseversa, por eso se usan los duplex stream para poder hacer operaciones
en el medio de ambos
 */

// "use strict";

const fs = require("fs");
const { Transform } = require("stream");

const pass = new Transform();
pass._transform = function transform(chunk, encoding, callback) {
  const data = chunk.toString("utf8");
  const modified = data.replace(/setTimeout/g, "setImmediate");
  callback(null, Buffer.from(modified));
};

const writable = fs.createWriteStream("callback_modified.dat");
const readable = fs.createReadStream("handler.js");

readable.pipe(pass).pipe(writable);
