"use strict";

const http = require("http");
const server = http.createServer();

server.on("request", (req, res) => {
  res.end("hello world");
});

server.on("connection", (socket) => {
  console.log(socket);
});

server.on("listening", () => {
  console.log("servidor escuchando");
});

server.listen(9999);
