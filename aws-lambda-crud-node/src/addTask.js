// in order to create an id
const { v4 } = require("uuid");
//in order to connect to AWS services
const AWS = require("aws-sdk");

const addTask = async (event) => {
  try {
    //in order to connect to Dynamodb
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const { title, description } = JSON.parse(event.body);
    const createdAt = new Date();
    const id = v4();

    const newTask = {
      id,
      title,
      description,
      createdAt,
      done: false,
    };
    //in order to save an object
    await dynamodb
      .put({
        TableName: "TaskTable",
        Item: newTask,
      })
      .promise();

    return {
      statusCode: 200,
      body: JSON.stringify(newTask),
    };
  } catch (e) {
    return e;
  }
};

module.exports = { addTask };
