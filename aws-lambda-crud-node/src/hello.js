"use strict";

module.exports.hello = async (event) => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: "hello from serverless",
        input: event,
      },
      null,
      2
    ),
  };
};

/**
 *serverless deploy --verbose : se usa para que al momento en que se vaya desplegandose la
me vaya mostrando mensajes en consola para ir viendo el proceso, se debe configurar primero
el client id y el secret para enlazarlo a AWS
- es importante instalar aws-sdk que nos permitira conectarnos a multiples servicios de AWS
 */
